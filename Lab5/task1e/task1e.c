#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/limits.h>
#include "LineParser.h"
#include <sys/types.h>
#include <sys/wait.h>
      
#define MAX_CMD 2048  


typedef struct history
{
    char * entry;
    struct history *next;	/* next history in chain */
} history;

history *hist_first = NULL;
history *hist_last = NULL;

void hist_print(){
  int num = 0;
  history *p = hist_first;
  
  while (p != NULL) {
    printf("#%d %s",num,p->entry);
    num++;
    p = p->next;
  } 
}

// returns new last
void hist_append(history *data){
  if (hist_first == NULL && hist_last == NULL) {
    hist_first = data;
    hist_last = data;
    return;
  }
  //printf("added:%s", hist_last->entry);
  hist_last->next = data;
  hist_last = data;
}
     
void hist_free(){
  history *p = hist_first;
  history *temp;
  
  while (p != NULL) {
    temp = p;
    p = p->next;
    free(temp->entry);
    free(temp);
  } 
}

void display_prompt() {
  char cwd[PATH_MAX];
  getcwd(cwd, PATH_MAX);
  printf("%s> ",cwd);
}

int is_end_cmd(cmdLine *pCmdLine) {
  if (strcmp(pCmdLine->arguments[0], "quit") == 0)
    return 1;
  return 0;
}

//  execv > replaces the current process image with a new process image.
void execute(cmdLine *pCmdLine) {
  int result;
  int pid;
  int status;
  
  if (strcmp(pCmdLine->arguments[0], "cd") == 0) {
    result = chdir(pCmdLine->arguments[1]);
    if (result == -1) {
      perror("Error ");
    }
    return;
  } else if (strcmp(pCmdLine->arguments[0], "history") == 0) {
    hist_print();
    return;
  }
  
  pid = fork();
  
  if (pid == 0) {
    result = execvp(pCmdLine->arguments[0], pCmdLine->arguments); 
    
    if (result == -1) {
      perror("Error ");
      _exit(1);
    }
  }
  else {
    if (pCmdLine->blocking == 1) {
      //printf("****Waiting*****");
      waitpid(pid, &status, 0);
    }
  }

}

char *get_history_entry(int num) {
  int i = 0;
  history *p = hist_first;
  
  while (i != num && p != NULL) {     
    i++;
    p = p->next;
  } 
  
  if (p == NULL) {
    printf("No such history record.\n");
    return "";
  }
  return p->entry;
}

int main(int argc, char **argv) {
  char cmdStr[MAX_CMD];
  cmdLine *cmd;
  history *new_entry;
  char *entry_str;
  
  while (1) {
    display_prompt();
    fgets(cmdStr, MAX_CMD, stdin);
    
    // history activation
    if (cmdStr[0] == '!') {
      strcpy(cmdStr, get_history_entry(atoi(cmdStr+1)));
    }
    
    cmd = parseCmdLines(cmdStr);

    if (cmd != NULL)
    {
      if (is_end_cmd(cmd)){
	freeCmdLines(cmd);
	break;
      }
      
      // history save
      new_entry = (history*) malloc (sizeof(history));
      entry_str = (char*) malloc (sizeof(char)*MAX_CMD);
      strcpy(entry_str, cmdStr);
      new_entry->entry = entry_str;
      new_entry->next = NULL;
      hist_append(new_entry);
      
      execute(cmd);
    }
    
    freeCmdLines(cmd);
  }
  
  hist_free();
  printf("bye\n");
  return 0;
}
