#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/limits.h>
#include "LineParser.h"
      
#define MAX_CMD 2048  

void display_prompt() {
  char cwd[PATH_MAX];
  getcwd(cwd, PATH_MAX);
  printf("%s> ",cwd);
}


int is_end_cmd(cmdLine *pCmdLine) {
  if (strcmp(pCmdLine->arguments[0], "quit") == 0)
    return 1;
  return 0;
}
      
void execute(cmdLine *pCmdLine) {
  int result;
  
  //  replaces the current process image with a new process image.
  result = execvp(pCmdLine->arguments[0], pCmdLine->arguments); 
  if (result == -1) {
    perror("Error ");
  }
}

int main(int argc, char **argv) {
  char cmdStr[MAX_CMD];
  cmdLine *cmd;
  
  while (1) {
    display_prompt();
    fgets(cmdStr, MAX_CMD, stdin);
    cmd = parseCmdLines(cmdStr);
    
    if (cmd != NULL)
    {
      if (is_end_cmd(cmd)){
	freeCmdLines(cmd);
	break;
      }
      
      execute(cmd);
      //printf("arg count: %d\n",cmd->argCount);
      
    }
    
    freeCmdLines(cmd);
  }
  
  printf("bye\n");
  return 0;
}
