#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/limits.h>
#include "LineParser.h"
#include <sys/types.h>
#include <sys/wait.h>
      
#define MAX_CMD 2048  

void display_prompt() {
  char cwd[PATH_MAX];
  getcwd(cwd, PATH_MAX);
  printf("%s> ",cwd);
}


int is_end_cmd(cmdLine *pCmdLine) {
  if (strcmp(pCmdLine->arguments[0], "quit") == 0)
    return 1;
  return 0;
}

//  execv > replaces the current process image with a new process image.
void execute(cmdLine *pCmdLine) {
  int result;
  int pid;
  int status;
  
  if (strcmp(pCmdLine->arguments[0], "cd") == 0) {
    result = chdir(pCmdLine->arguments[1]);
    if (result == -1) 
      perror("Error ");
    return;
  } 
  
  pid = fork();
  
  if (pid == 0) {
    result = execvp(pCmdLine->arguments[0], pCmdLine->arguments); 
    
    if (result == -1) {
      perror("Error ");
      _exit(1);
    }
  }
  else {
    if (pCmdLine->blocking == 1) {
      //printf("****Waiting*****");
      waitpid(pid, &status, 0);
    }
  }

}

int main(int argc, char **argv) {
  char cmdStr[MAX_CMD];
  cmdLine *cmd;
  
  while (1) {
    display_prompt();
    fgets(cmdStr, MAX_CMD, stdin);
    cmd = parseCmdLines(cmdStr);
    
    if (cmd != NULL)
    {
      if (is_end_cmd(cmd)){
	freeCmdLines(cmd);
	break;
      }
      
      execute(cmd);
      //printf("arg count: %d\n",cmd->argCount);
      
    }
    
    freeCmdLines(cmd);
  }
  
  printf("bye\n");
  return 0;
}
