#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/wait.h>
    


int main(int argc, char **argv) {
  int pipefd[2];
  int pid, pid2;
  int result;
  int status;
  char *const args1[]   = {"ls","-l",NULL};
  char *const args2[]  = {"tail","-n","2",NULL};
  
  // create pipe
  if (pipe(pipefd) == -1) {
      perror("pipe");
      exit(1);
  }
  
  // fork
  pid = fork();
  
  if (pid == 0) { // Child process
    close(1); //stdout
    
    dup(pipefd[1]); //write-end
    
    close(pipefd[1]);
    
    result = execvp("ls", args1); 
    
    if (result == -1) {
      perror("Error ");
      _exit(1);
    }
  } else { // Parent process
    close(pipefd[1]);
    
    // fork again
    pid2 = fork();
    
    if (pid2 == 0) { //child #2
      close(0); //stdin
      
      dup(pipefd[0]); //read-end
      
      close(pipefd[0]);
      
      result = execvp("tail", args2); 
      
    } else { // Parent process
      close(pipefd[0]); //read-end
      
      waitpid(pid, &status, 0);
      waitpid(pid2, &status, 0);
      
    }

  }
  
  return 0;
}
