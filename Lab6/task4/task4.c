#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/limits.h>
#include "LineParser.h"
#include <sys/types.h>
#include <sys/wait.h>
      
#define MAX_CMD 2048  

typedef struct env
{
    char * name;
    char * value;
    struct env *next;	/* next in chain */
} env;

env *env_first = NULL;
env *env_last = NULL;

typedef struct history
{
    char * entry;
    struct history *next;	/* next in chain */
} history;

history *hist_first = NULL;
history *hist_last = NULL;

void env_print(){
  env *p = env_first;
  
  while (p != NULL) {
    printf("$%s = %s\n",p->name,p->value);
    p = p->next;
  } 
}

void hist_print(){
  int num = 0;
  history *p = hist_first;
  
  while (p != NULL) {
    printf("#%d %s",num,p->entry);
    num++;
    p = p->next;
  } 
}

// returns new last
void hist_append(history *data){  
  if (hist_first == NULL && hist_last == NULL) {
    hist_first = data;
    hist_last = data;
    return;
  }
  //printf("added:%s", hist_last->entry);
  hist_last->next = data;
  hist_last = data;
}

void env_append(env *data){
  if (env_first == NULL && env_last == NULL) {
    env_first = data;
    env_last = data;
    return;
  }
  
  env_last->next = data;
  env_last = data;
}
     
void hist_free(){
  history *p = hist_first;
  history *temp;
  
  while (p != NULL) {
    temp = p;
    p = p->next;
    free(temp->entry);
    free(temp);
  } 
}

void env_free(){
  env *p = env_first;
  env *temp;
  
  while (p != NULL) {
    temp = p;
    p = p->next;
    free(temp->name);
    free(temp->value);
    free(temp);
  } 
}

void display_prompt() {
  char cwd[PATH_MAX];
  getcwd(cwd, PATH_MAX);
  printf("%s> ",cwd);
}

int is_end_cmd(cmdLine *pCmdLine) {
  if (strcmp(pCmdLine->arguments[0], "quit") == 0)
    return 1;
  return 0;
}

char *get_history_entry(int num) {
  int i = 0;
  history *p = hist_first;
  
  while (i != num && p != NULL) {     
    i++;
    p = p->next;
  } 
  
  if (p == NULL) {
    printf("No such history record.\n");
    return "";
  }
  return p->entry;
}

env *get_env(char *name) {
  env *p = env_first;
  
  while (p != NULL) {
    if (strcmp(p->name,name) == 0) 
      return p;
    
    p = p->next;
  }
  
  return NULL;
}

void delete_env(char *name) {
  env *p = env_first;
  env *prev = NULL;
  
  while (p != NULL) {  
    if (strcmp(p->name,name) == 0) {
      if (env_last == p)
	env_last = prev;
      
      if (prev == NULL) {
	env_first = p->next;
      } else {
	prev->next = p->next;
      }
      
      free(p->name);
      free(p->value);
      free(p);
      return;
    }
    
    prev = p;
    p = p->next;
  }

  printf("Could not find variable: %s\n", name);
}

void redirect(cmdLine *pCmdLine){
  if (pCmdLine->inputRedirect != NULL) {
    close(0);
    fopen(pCmdLine->inputRedirect,"r");
  }
  
  if (pCmdLine->outputRedirect != NULL) {
    close(1);
    fopen(pCmdLine->outputRedirect,"w+");
  }
}


int **createPipes(int nPipes) {
  int **pipefd;
  int i;
  
  if (nPipes == 0)
    return NULL;
  
  pipefd = (int**) malloc (sizeof(int*)*nPipes);
  
  for (i = 0; i < nPipes; i++) {
    *(pipefd + i) = (int*) malloc (sizeof(int)*2);
    
    if (pipe(*(pipefd + i)) == -1) {
      perror("pipe");
      exit(1);
    }
  }
  
  return pipefd;
}

void releasePipes(int **pipes, int nPipes) {
  int i;
  
  if (nPipes == 0 || pipes == NULL)
    return;
  
  for (i = 0; i < nPipes; i++) {
    free (*(pipes + i));
  }
  
  free (pipes);
}

int *leftPipe(int **pipes, cmdLine *pCmdLine) {
  int i;
  
  if (pCmdLine->idx <= 0) {
    return NULL;
  }
  
  i = pCmdLine->idx - 1;
  
  return *(pipes + i);
  
}

int *rightPipe(int **pipes, cmdLine *pCmdLine) {
  int i;
  
  if (pCmdLine->next == NULL) {
    return NULL;
  }
  
  i = pCmdLine->idx;
  
  return *(pipes + i);
}

int countLines(cmdLine *pCmdLine) {
  cmdLine *p = pCmdLine;
  int i = 0;
  
  while (p != NULL) {
    i++;
    p = p->next;
  }
  
  return i;
}

//  execv > replaces the current process image with a new process image.
void execute(cmdLine *pCmdLine, int** pipes) {
  int result;
  int pid, pidPipe;
  int status;
  env *temp_env;
  cmdLine *p = pCmdLine->next;
  
  // special cmds
  if (strcmp(pCmdLine->arguments[0], "cd") == 0) {
    result = chdir(pCmdLine->arguments[1]);
    if (result == -1) {
      perror("Error ");
    }
    return;
  } else if (strcmp(pCmdLine->arguments[0], "history") == 0) {
    hist_print();
    return;
  } else if (strcmp(pCmdLine->arguments[0], "set") == 0) {
    temp_env = get_env(pCmdLine->arguments[1]);
    
    if (temp_env == NULL) {
      // add new env
      temp_env = (env*) malloc (sizeof(env));
      temp_env->name = (char*) malloc (sizeof(char)*MAX_CMD);
      strcpy(temp_env->name, pCmdLine->arguments[1]);
      temp_env->value = (char*) malloc (sizeof(char)*MAX_CMD);
      strcpy(temp_env->value, pCmdLine->arguments[2]);
      temp_env->next = NULL;
      env_append(temp_env);
    }
    else {
      // update
      strcpy(temp_env->value, pCmdLine->arguments[2]);
    }
    return;
  } else if (strcmp(pCmdLine->arguments[0], "env") == 0) {
    env_print();
    return;
  } else if (strcmp(pCmdLine->arguments[0], "rename") == 0) {
    temp_env = get_env(pCmdLine->arguments[1]);
    
    if (temp_env != NULL) {
      strcpy(temp_env->name,pCmdLine->arguments[2]);
    } else {
      printf("Could not find variable: %s\n", pCmdLine->arguments[1]);
    }
    return;
  }else if (strcmp(pCmdLine->arguments[0], "delete") == 0) {
    delete_env(pCmdLine->arguments[1]);
    return;
  }
  
  //pipes = createPipes(countLines(pCmdLine));
  
  // duplicate the process
  pid = fork();
  
  if (pid == 0) { // Child process #1 (writing to pipe)
    redirect(pCmdLine);
    
    if (p != NULL){ //has pipes
      close(1); //stdout
      dup(rightPipe(pipes, pCmdLine)[1]); //write-end
      close(rightPipe(pipes, pCmdLine)[1]);
    }

    result = execvp(pCmdLine->arguments[0], pCmdLine->arguments); 
      
    if (result == -1) {
      perror("Error ");
      _exit(1);
    }
    
  } else { // Parent process
    
    while (p != NULL) { //has pipes
      close(leftPipe(pipes, p)[1]);
      
      // fork again
      pidPipe = fork();
      
      if (pidPipe == 0) { //child #2 (Reading from pipe)
	redirect(p);
	
	close(0); //stdin
	dup(leftPipe(pipes, p)[0]); //read-end
	close(leftPipe(pipes, p)[0]);
	
	//check for right pipe
	if (rightPipe(pipes, p) != NULL){ //has right pipes
	  close(1); //stdout
	  dup(rightPipe(pipes, p)[1]); //write-end
	  close(rightPipe(pipes, p)[1]);
	}
	
	result = execvp(p->arguments[0], p->arguments); 
  
	if (result == -1) {
	  perror("Error ");
	  _exit(1);
	}
      } else { // Super Parent process
	close(leftPipe(pipes, p)[0]); //read-end
	waitpid(pidPipe, &status, 0);
      }
      
      p = p->next;
    }
      
    if (pCmdLine->blocking == 1) {
      waitpid(pid, &status, 0);
    }
  }
}

void replace_vars(cmdLine *cmd) {
  int i;
  env *temp;
  cmdLine *p = cmd;
  
  while (p != NULL) {
    for (i = 0; i < p->argCount; i++) {
      if (p->arguments[i][0] == '$') {
	temp = get_env(p->arguments[i] + 1);
	
	if (temp == NULL) {
	  printf("Could not find variable: %s\n", p->arguments[i] + 1);
	  replaceCmdArg(p, i, "");
	} else {
	  replaceCmdArg(p, i, temp->value);
	}
      }
    }
    p = p->next;
  }
}

int main(int argc, char **argv) {
  char cmdStr[MAX_CMD];
  cmdLine *cmd;
  history *new_entry;
  char *entry_str;
  int **pipes;
  
  while (1) {
    display_prompt();
    fgets(cmdStr, MAX_CMD, stdin);
    
    // history activation
    if (cmdStr[0] == '!') {
      strcpy(cmdStr, get_history_entry(atoi(cmdStr+1)));
    }
    
    cmd = parseCmdLines(cmdStr);

    if (cmd != NULL)
    {
      if (is_end_cmd(cmd)){
	freeCmdLines(cmd);
	break;
      }
      
      // history save
      new_entry = (history*) malloc (sizeof(history));
      entry_str = (char*) malloc (sizeof(char)*MAX_CMD);
      strcpy(entry_str, cmdStr);
      new_entry->entry = entry_str;
      new_entry->next = NULL;
      hist_append(new_entry);
      
      replace_vars(cmd);
      
      // create pipes, exec, release pipes
      pipes = createPipes(countLines(cmd) - 1);

      execute(cmd, pipes);
      
      releasePipes(pipes,countLines(cmd) - 1);
    }
    
    freeCmdLines(cmd);
  }
  
  env_free();
  hist_free();
  printf("bye\n");
  return 0;
}
