﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define ALLOC_BYTES 4096
  
struct fun_desc {
  char *name;
  void (*fun)(char*, int, void*);
}; 

void printHex(int address, int length, int size) {
  int i;
  unsigned char *byte = (unsigned char*)address;
  
  for(i = 0; i < length; ++i) {
    if (i != 0 && (i % (size)) == 0)
      printf(" "); 
    
    printf("%02X" ,byte[i]);
  }
  
  printf("\n");
}
 
void display_mem(char *filename, int size, void *mem_buffer) {
  char input[16];
  int length;
  int address;
  
  printf("Please enter <address-hex> <length-dec>\n");
  
  fgets(input, 16, stdin);
  sscanf(input, "%x %d", &address, &length);
  
  if (address == 0) 
    address = (int) mem_buffer;
  
  printHex(address, length, size);
}

void load_mem(char *filename, int size, void *mem_buffer) {
  char input[256];
  int length, address, location, i = 0;
  char path[256];
  char c;
  FILE *sFile;
  unsigned char *byte;
  
  printf("Please enter <mem-address> <source-file> <location> <length>\n");
  
  fgets(input, 128, stdin);
  sscanf(input, "%x %s %x %d", &address, path, &location, &length);

  if (address == 0) 
    address = (int) mem_buffer;
  
   byte = (unsigned char*)address;
  
  // open file
  sFile = fopen ( path, "rb" );
  if (sFile==NULL) {fputs ("File error\n",stderr); exit (1);}

  // goto needed location
  fseek (sFile , location , SEEK_SET);
  
  c = fgetc(sFile);
  while (i < length * size) {
      byte[i] = c;
      i++;
      c = fgetc (sFile);
  } 
    
  fclose(sFile);

  printf("Loaded %d units into 0x%x\n", length, address);
}

void save(char *filename, int size, void *mem_buffer) {
  
}

void quit(char *filename, int size, void *mem_buffer) {
  free(mem_buffer);
  printf("Bye.\n");
  exit(0);
}

void showMenu(struct fun_desc menu[]) {
  int i = 0;
  
  printf("Please choose a function:\n");
  
  while (menu[i].name != NULL) {
    printf("%d) %s\n", i, menu[i].name);
    i++;
  }
  
  printf("Option: ");
}

 
int main(int argc, char **argv){
  char input[10];
  char *fileName;
  int unitSize = 1;
  int op;
  void *mem_buffer;
  
  struct fun_desc menu[] = { 
  { "Mem Display", display_mem }, 
  { "Load Into Memory", load_mem }, 
  { "Save Into File", save } , 
  { "Quit", quit } ,
  { NULL , NULL}  };
   
  // Reading params
  if (argc == 2) {
    fileName = argv[1];
  } else if (argc == 3) {
    fileName = argv[1];
    unitSize = atoi(argv[2]);
    if (unitSize != 1 && unitSize != 2 && unitSize != 4) {
      printf("Wrong unit size.\n");
      return 1;
    }
  } else {
    printf("Wrong number of params.\n");
    return 1;
  }

  mem_buffer = (void*) malloc(ALLOC_BYTES);
  
  printf("File: %s, buffer location: 0x%x\n", fileName, (int) mem_buffer);

  // Read input & execute loop
  while (1) {
    showMenu(menu);
    
    fgets(input, 10, stdin);
    op = atoi(input);
    
    // exec
    if (op < 4) {
      menu[op].fun(fileName, unitSize, mem_buffer);
    }
    else
      printf("invalid option. try again.\n");
  
  }
  return 0;
}