
int digit_cnt(char* string) {
  int i = 0;
  int count = 0;
  
  while (string[i] != '\0') {
    if (string[i] > 47 && string[i] < 58) {
      count++;
    }
    
    i++;
  }
  
  return count;
}

