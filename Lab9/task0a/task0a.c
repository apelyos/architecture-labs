#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <elf.h>

int main(int argc, char **argv){
  int fd, i;
  void *map_start; /* will point to the start of the memory mapped file */
  char magicNum[3];
  struct stat fd_stat; /* this is needed to  the size of the file */
  Elf32_Ehdr *header; /* this will point to the header structure */
  Elf32_Phdr *phoff_p;
  int num_of_program_headers;

  if( (fd = open(argv[1], O_RDWR)) < 0 ) {
    perror("error in open");
    exit(-1);
  }

  if( fstat(fd, &fd_stat) != 0 ) {
    perror("stat failed");
    exit(-1);
  }

  if ( (map_start = mmap(0, fd_stat.st_size, PROT_READ , MAP_PRIVATE, fd, 0)) == MAP_FAILED) {
    perror("mmap failed");
    exit(-4);
  }


  header = (Elf32_Ehdr *) map_start;

  strncpy(magicNum, (char*) (header->e_ident + 1), 3);
  
  if (strncmp(magicNum, "ELF", 3) != 0) {
    printf("Invalid ELF file.\n");
    exit(-5);
  }
  

  
  if (header->e_phoff == 0) {
    printf("Has no PHs.\n");
    exit(-6);
  }
  
  phoff_p = (Elf32_Phdr *) (map_start + header->e_phoff); // Program header
  num_of_program_headers = header->e_phnum;
  
  for (i = 0 ; i < num_of_program_headers ; i++) {
    printf("%08x\t%08x\t%08x\t%08x\t%08x\t%08x\t%d\t%x\n",phoff_p[i].p_type, phoff_p[i].p_offset,
	   phoff_p[i].p_vaddr,phoff_p[i].p_paddr,phoff_p[i].p_filesz, phoff_p[i].p_memsz, 
	   phoff_p[i].p_flags, phoff_p[i].p_align   );
  }
  


  /* now, we unmap the file */
  munmap(map_start, fd_stat.st_size);
  
  close(fd);
  
  return 0;
} 
