﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>
 
char censor(char c) {
  if(c == '!') {
    return '.';
  }
  return c;
}
 
char to_lower(char c) {
  if(c < 91 && c > 64) {
    return c + 32;
  }
  
  return c;
}

char cprt(char c) {
  //printf("cprt: %c=%d\n", c,(int)c); 
  
  if (c != 0)
    printf("%c\n", c);
  
  return c;
}

char my_get(char c) {
  char input = fgetc(stdin);
  
  if (input != '\n' && input != EOF) {
    //printf("got: %c=%d\n", input,(int)input);
    return input;
  }
  
  return 0;
}

void for_each(char *array, char (*f) (char)){
  int i = 0;
  char c = f(array[0]);
  
  while (c != 0)
  { 
    array[i] = c;
    i++;
    c = f(array[i]);
  } 
}

 
int main(int argc, char **argv){
  char c[5] = "";
  for_each(c, my_get);
  for_each(c, cprt);
  for_each(c, to_lower);
  for_each(c, censor);
  for_each(c, cprt);
  return 0;
}