﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>
  
struct fun_desc {
  char *name;
  char (*fun)(char);
}; 
 


char censor(char c) {
  if(c == '!') {
    return '.';
  }
  return c;
}
 
char to_lower(char c) {
  if(c < 91 && c > 64) {
    return c + 32;
  }
  
  return c;
}

char cprt(char c) {
  //printf("cprt: %c=%d\n", c,(int)c); 
  
  if (c != 0)
    printf("%c\n", c);
  
  return c;
}

char my_get(char c) {
  char input = fgetc(stdin);
  
  if (input != '\n' && input != EOF) {
    //printf("got: %c=%d\n", input,(int)input);
    return input;
  }
  
  return 0;
}

char quit(char c) {
  exit(0);
}

void for_each(char *array, char (*f) (char)){
  int i = 0;
  char c = f(array[0]);
  
  while (c != 0)
  { 
    array[i] = c;
    i++;
    c = f(array[i]);
  } 
}

void showMenu(struct fun_desc menu[]) {
  int i = 0;
  
  printf("Please choose a function:\n");
  
  while (menu[i].name != NULL) {
    printf("%d) %s\n", i, menu[i].name);
    i++;
  }
  
  printf("Option: ");
  //fflush(stdout);
}

 
int main(int argc, char **argv){
  char carray[100] = "";
  char input[10];
  int op;

  struct fun_desc menu[] = { 
  { "To Lower Case", to_lower }, 
  { "Censor", censor }, 
  { "Print", cprt } , 
  { "Get String", my_get } , 
  { "Quit", quit } ,
  { NULL , NULL}  };
  
  while (1) {
    showMenu(menu);
    
    fgets(input, 10, stdin);
    op = atoi(input);
    
    if (op < 6) {
      for_each(carray, menu[op].fun);
    }
    else
      printf("invalid option. try again.\n");
  
  }
  return 0;
}