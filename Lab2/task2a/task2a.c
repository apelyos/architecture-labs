﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>
 
char censor(char c) {
  if(c == '!') {
    return '.';
  }
  return c;
}

void for_each(char *array, char (*f) (char)){
  int i = 0;
  char c = f(array[0]);
  
  while (c != 0)
  { 
    *(array+i) = c;
    i++;
    c = f(array[i]);
  } 
}

 
int main(int argc, char **argv){
  char c[] = {'H','E','Y','!', 0};
  for_each(c, censor);
  printf("%s\n", c); // HEY.
  return 0;
}