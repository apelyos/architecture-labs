// All rights reserved to Yos Apel (C) 2015

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  int c, i;
  int line = 1;
  int splitByChar = -1;
  int splitByDigit = 0;
  
  // Reading params
  for(i=1; i<argc; i++){
    if(strcmp(argv[i],"-s")==0)
	splitByChar = argv[++i][0];
    else if(strcmp(argv[i],"-d")==0)
	splitByDigit = 1;
    else {
	printf("invalid parameter - %s\n",argv[i]);
	return 1;
    }
  }
  
  // Starting to split stuff
  c = fgetc(stdin);
  printf("%d:",line);
  
  while (c != -1) {
    printf("%c",c);
    
    switch(c) {
      case '@':
      case '*':
	line++;
	printf("\n%d:",line);
	break;
    }
    
    if (splitByChar != -1) {
      if (c == splitByChar) {
	line++;
	printf("\n%d:",line);
      }
    }
    
    if (splitByDigit) {
      if (c >= 48 && c <= 57) {
	line++;
	printf("\n%d:",line);
      }
    }
    
    c = fgetc(stdin);
  } 
  
  //printf("terminating!\n");
}