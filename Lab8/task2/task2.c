#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <elf.h>

int main(int argc, char **argv){
  int fd, i;
  void *map_start; /* will point to the start of the memory mapped file */
  char magicNum[3];
  struct stat fd_stat; /* this is needed to  the size of the file */
  Elf32_Ehdr *header; /* this will point to the header structure */
  Elf32_Shdr *section;
  Elf32_Sym  *symbol;
  char * sh_strtab_p;
  char * strtab_p;
  int num_of_section_headers, num_of_symbols, sym_idx = 0, strtab_idx = 0, dyn_sym_idx = 0, dyn_strtab_idx = 0;

  if( (fd = open(argv[1], O_RDWR)) < 0 ) {
    perror("error in open");
    exit(-1);
  }

  if( fstat(fd, &fd_stat) != 0 ) {
    perror("stat failed");
    exit(-1);
  }

  if ( (map_start = mmap(0, fd_stat.st_size, PROT_READ , MAP_PRIVATE, fd, 0)) == MAP_FAILED) {
    perror("mmap failed");
    exit(-4);
  }

  /* now, the file is mapped starting at map_start.
  * all we need to do is tell *header to point at the same address:
  */

  header = (Elf32_Ehdr *) map_start;
  /* now we can do whatever we want with header!!!!
  * for example, the number of section header can be obtained like this:
  */
  
  strncpy(magicNum, (char*) (header->e_ident + 1), 3);
  printf("Magic number first 3 bytes: %c%c%c\n", magicNum[0],magicNum[1],magicNum[2] );
  
  if (strncmp(magicNum, "ELF", 3) != 0) {
    printf("Invalid ELF file.\n");
    exit(-5);
  }
  
  printf("Data encoding scheme: ");
  if (header->e_ident[EI_DATA] == 1) {
    printf("2's complement, little endian\n");
  } else if (header->e_ident[EI_DATA] == 1) {
    printf("2's complement, big endian\n");
  } else {
    printf("Invalid encoding.\n");
    exit(-6);
  }
  
  printf("Entry point: 0x%x\n", header->e_entry);
  printf("Section header offset: 0x%x\n", header->e_shoff);
  
  num_of_section_headers = header->e_shnum;
  printf("Number of section headers: %d\n", num_of_section_headers);  
  printf("\tSize of each: %d\n", header->e_shentsize);  
  printf("Program header offset: 0x%x\n", header->e_phoff);  
  printf("Number of program headers: %d\n", header->e_phnum);  
  printf("\tSize of each: %d\n", header->e_phentsize);
  
  section = (Elf32_Shdr *) (map_start + header->e_shoff); // section table start addr
  
  sh_strtab_p = map_start + section[header->e_shstrndx].sh_offset; // SH string table location
  
  printf("\nSection headers:\n");
  for (i = 0 ; i < num_of_section_headers ; i++) {
    printf ("[%02d] %s\t%08x\t%08x\t%d\n" , i, sh_strtab_p + section[i].sh_name, 
	    section[i].sh_addr, section[i].sh_offset, section[i].sh_size);
    
    if (section[i].sh_type == SHT_SYMTAB)
    {
      sym_idx = i;
      //printf("Symbol table index1: %d\n",sym_idx);
      strtab_idx = section[i].sh_link;
      //printf("String table index1: %d\n",section[i].sh_link);
    }
    
     if (section[i].sh_type == SHT_DYNSYM)
    {
      dyn_sym_idx = i;
      //printf("Symbol table index1: %d\n",dyn_sym_idx);
      dyn_strtab_idx = section[i].sh_link;
      //printf("String table index1: %d\n",section[i].sh_link);
    }
  }
  
  if (sym_idx == 0) {
    printf("Could not find sym idx.\n");
    exit(-7);
  }
  
  symbol = (Elf32_Sym *) (map_start + section[dyn_sym_idx].sh_offset); // symbol table start addr
  
  num_of_symbols = section[dyn_sym_idx].sh_size / sizeof(Elf32_Sym);//16; // 16 = symbol entry size
  
  strtab_p = map_start + section[dyn_strtab_idx].sh_offset; //string table location
  
  printf("\n Dynamic Symbol names:\n");
  for (i = 0 ; i < num_of_symbols ; i++) {
    if (symbol[i].st_shndx == SHN_ABS) {
      printf ("[%02d] %08x\t%s\t%s\t%s\n" , i, symbol[i].st_value, "ABS", 
	      "ABS", strtab_p + symbol[i].st_name);
    } else if (symbol[i].st_shndx == SHN_UNDEF) {
      printf ("[%02d] %08x\t%s\t%s\t%s\n" , i, symbol[i].st_value, "UND", 
	      "UND", strtab_p + symbol[i].st_name);
    } else {
      printf ("[%02d] %08x\t%d\t%s\t%s\n" , i, symbol[i].st_value, symbol[i].st_shndx, 
	      sh_strtab_p + section[symbol[i].st_shndx].sh_name, strtab_p + symbol[i].st_name);
    } 
  }
  
  symbol = (Elf32_Sym *) (map_start + section[sym_idx].sh_offset); // symbol table start addr
  
  num_of_symbols = section[sym_idx].sh_size / sizeof(Elf32_Sym);//16; // 16 = symbol entry size
  
  strtab_p = map_start + section[strtab_idx].sh_offset; //string table location
  
  printf("\nSymbol names:\n");
  for (i = 0 ; i < num_of_symbols ; i++) {
    if (symbol[i].st_shndx == SHN_ABS) {
      printf ("[%02d] %08x\t%s\t%s\t%s\n" , i, symbol[i].st_value, "ABS", 
	      "ABS", strtab_p + symbol[i].st_name);
    } else if (symbol[i].st_shndx == SHN_UNDEF) {
      printf ("[%02d] %08x\t%s\t%s\t%s\n" , i, symbol[i].st_value, "UND", 
	      "UND", strtab_p + symbol[i].st_name);
    } else {
      printf ("[%02d] %08x\t%d\t%s\t%s\n" , i, symbol[i].st_value, symbol[i].st_shndx, 
	      sh_strtab_p + section[symbol[i].st_shndx].sh_name, strtab_p + symbol[i].st_name);
    } 
  }

  /* now, we unmap the file */
  munmap(map_start, fd_stat.st_size);
  
  close(fd);
  
  return 0;
} 
