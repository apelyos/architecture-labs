﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <elf.h>

int main(int argc, char **argv){
  int fd;
  void *map_start; /* will point to the start of the memory mapped file */
  char magicNum[3];
  struct stat fd_stat; /* this is needed to  the size of the file */
  Elf32_Ehdr *header; /* this will point to the header structure */
  //Elf32_Shdr *sections;
  int num_of_section_headers;

  if( (fd = open(argv[1], O_RDWR)) < 0 ) {
    perror("error in open");
    exit(-1);
  }

  if( fstat(fd, &fd_stat) != 0 ) {
    perror("stat failed");
    exit(-1);
  }

  if ( (map_start = mmap(0, fd_stat.st_size, PROT_READ | PROT_WRITE , MAP_SHARED, fd, 0)) <0 ) {
    perror("mmap failed");
    exit(-4);
  }

  /* now, the file is mapped starting at map_start.
  * all we need to do is tell *header to point at the same address:
  */

  header = (Elf32_Ehdr *) map_start;
  /* now we can do whatever we want with header!!!!
  * for example, the number of section header can be obtained like this:
  */
  
  strncpy(magicNum, (char*) (header->e_ident + 1), 3);
  printf("Magic number first 3 bytes: %c%c%c\n", magicNum[0],magicNum[1],magicNum[2] );
  
  if (strncmp(magicNum, "ELF", 3) != 0) {
    printf("Invalid ELF file.\n");
    exit(-5);
  }
  
  printf("Data encoding scheme: ");
  if (header->e_ident[EI_DATA] == 1) {
    printf("2's complement, little endian\n");
  } else if (header->e_ident[EI_DATA] == 1) {
    printf("2's complement, big endian\n");
  } else {
    printf("Invalid encoding.\n");
    exit(-6);
  }
  
  printf("Entry point: 0x%x\n", header->e_entry);
  printf("Section header offset: 0x%x\n", header->e_shoff);
  
  num_of_section_headers = header->e_shnum;
  printf("Number of section headers: %d\n", num_of_section_headers);  
  printf("\tSize of each: %d\n", header->e_shentsize);  
  printf("Program header offset: 0x%x\n", header->e_phoff);  
  printf("Number of program headers: %d\n", header->e_phnum);  
  printf("\tSize of each: %d\n", header->e_phentsize);

  
  /* now, we unmap the file */
  munmap(map_start, fd_stat.st_size);
  
  close(fd);
  
  return 0;
}