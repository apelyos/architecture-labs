#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <elf.h>

#define MAX_SYMBOLS	10000


typedef struct
{
  char *name;
  int isDefined;
} SymbolInfo;

void getSymbols(void *map_start, SymbolInfo symbolInfo[], int fd) {
  int i;
  //void *map_start; 
  char magicNum[3];
  struct stat fd_stat; /* this is needed to  the size of the file */
  Elf32_Ehdr *header; /* this will point to the header structure */
  Elf32_Shdr *section;
  Elf32_Sym  *symbol;
  char * strtab_p;
  int num_of_section_headers, num_of_symbols, sym_idx = 0, strtab_idx = 0;

  if( fstat(fd, &fd_stat) != 0 ) {
    perror("stat failed");
    exit(-1);
  }

  if ( (map_start = mmap(0, fd_stat.st_size, PROT_READ , MAP_PRIVATE, fd, 0)) == MAP_FAILED) {
    perror("mmap failed");
    exit(-4);
  }

  header = (Elf32_Ehdr *) map_start;
  
  strncpy(magicNum, (char*) (header->e_ident + 1), 3);

  if (strncmp(magicNum, "ELF", 3) != 0) {
    printf("Invalid ELF file.\n");
    exit(-5);
  }
  
  num_of_section_headers = header->e_shnum;
 
  section = (Elf32_Shdr *) (map_start + header->e_shoff); // section table start addr
    
  for (i = 0 ; i < num_of_section_headers ; i++) {
    if (section[i].sh_type == SHT_SYMTAB)
    {
      sym_idx = i;
      strtab_idx = section[i].sh_link;
    }
  }
  
  if (sym_idx == 0) {
    printf("Could not find sym idx.\n");
    exit(-7);
  }
  
  symbol = (Elf32_Sym *) (map_start + section[sym_idx].sh_offset); // symbol table start addr
  
  num_of_symbols = section[sym_idx].sh_size / sizeof(Elf32_Sym); // 16 = symbol entry size
  
  strtab_p = map_start + section[strtab_idx].sh_offset; //string table location
  
  for (i = 0 ; i < num_of_symbols ; i++) {
    if (symbol[i].st_shndx == SHN_ABS) {
      symbolInfo[i].name = strtab_p + symbol[i].st_name;
      symbolInfo[i].isDefined = 1;
    } else if (symbol[i].st_shndx == SHN_UNDEF) {
      symbolInfo[i].name = strtab_p + symbol[i].st_name;
      symbolInfo[i].isDefined = 0;
    } else {
      symbolInfo[i].name = strtab_p + symbol[i].st_name;
      symbolInfo[i].isDefined = 1;
    } 
  }
  
  
}

void unmap(void* map_start, int fd) {
  struct stat fd_stat;
  
  if( fstat(fd, &fd_stat) != 0 ) {
    perror("stat failed");
    exit(-1);
  }
  
  munmap(map_start, fd_stat.st_size);
}

int hasStart(SymbolInfo symbolInfo[]) {
  int i;
  
  for (i = 0 ; i < MAX_SYMBOLS &&  symbolInfo[i].isDefined != -1; i++) {
    if (strcmp(symbolInfo[i].name , "_start") == 0){
      return 1;
    }
  }
  
  return 0;
}

char* checkDup(SymbolInfo symbolInfo1[], SymbolInfo symbolInfo2[]) {
  int i,j;
  
  for (i = 0 ; i < MAX_SYMBOLS &&  symbolInfo1[i].isDefined != -1; i++) {
      for (j = 0 ; i < MAX_SYMBOLS &&  symbolInfo2[j].isDefined != -1; j++) {
	if (symbolInfo1[i].isDefined && symbolInfo2[j].isDefined && 
	    strcmp(symbolInfo1[i].name , "") != 0 &&
	    strcmp(symbolInfo1[i].name , symbolInfo2[j].name) == 0){
	  return symbolInfo1[i].name;
	}
      }
  }
  return NULL;
}

char* checkUndef(SymbolInfo symbolInfo1[], SymbolInfo symbolInfo2[]) {
  int i,j;
  int isDef = 0;
  
  for (i = 0 ; i < MAX_SYMBOLS &&  symbolInfo1[i].isDefined != -1; i++) {
    if (symbolInfo1[i].isDefined == 0 && strcmp(symbolInfo1[i].name , "") != 0) { //undefined
      //printf ("undef: %s\n", symbolInfo1[i].name);
      for (j = 0 ; i < MAX_SYMBOLS &&  symbolInfo2[j].isDefined != -1; j++) {
	if (strcmp(symbolInfo1[i].name , symbolInfo2[j].name) == 0){
	  isDef = 1;
	}
      }
      
      if (isDef == 0)
	return symbolInfo1[i].name;
      
    }
    isDef = 0;
  }
  
  return NULL;
}

void initInfo(SymbolInfo symbolInfo[]) {
  int i;
  
  for (i = 0 ; i < MAX_SYMBOLS ; i++) {
    symbolInfo[i].name = NULL;
    symbolInfo[i].isDefined = -1;
  }
}


int main(int argc, char **argv){
  void *map_start1 = NULL; 
  void *map_start2 = NULL; 
  SymbolInfo symbolInfo1[MAX_SYMBOLS];
  SymbolInfo symbolInfo2[MAX_SYMBOLS];
  int fd1, fd2;
  char *dup;
  char *undef;
  
  initInfo(symbolInfo1);
  initInfo(symbolInfo2);
  
  if( (fd1 = open(argv[1], O_RDWR)) < 0 ) {
    perror("error in open");
    exit(-1);
  }
  
  if( (fd2 = open(argv[2], O_RDWR)) < 0 ) {
    perror("error in open");
    exit(-1);
  }
  
  getSymbols(map_start1, symbolInfo1, fd1);
  getSymbols(map_start2, symbolInfo2, fd2);
  
  if (hasStart(symbolInfo2) || hasStart(symbolInfo1)) {
    printf("_start check: PASSED\n");
  } else {
    printf("_start check: FAILED\n");
    exit(-1);
  }
  
  dup = checkDup(symbolInfo1,symbolInfo2);
  if (dup == NULL)  {
    printf("duplicate check: PASSED\n");
  } else {
    printf("duplicate check: FAILED (%s)\n",dup);
    exit(-1);
  }
  
  // both ways
  undef = checkUndef(symbolInfo1,symbolInfo2);
  if (undef == NULL)  {
    undef = checkUndef(symbolInfo2,symbolInfo1);
      if (undef == NULL)  {
	printf("no missing symbols: PASSED\n");
      } else {
	printf("no missing symbols: FAILED (%s)\n",undef);
	exit(-1);
      }
  } else {
    printf("no missing symbols: FAILED (%s)\n",undef);
    exit(-1);
  }
  
  unmap(map_start1, fd1);
  unmap(map_start2, fd2);
  close(fd1);
  close(fd2);
  
  return 0;
}