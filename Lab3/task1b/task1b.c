#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

typedef struct virus virus;
 
struct virus {
        int length;
        char *signature;
        char *name;
        virus *next;
};

void PrintHex(buffer, length) {
  int i;
  unsigned char *byte = (unsigned char*)buffer;
  
  for(i = 0; i<length; ++i){
    printf("%02X " ,byte[i]);
  }
  
}

 /* Print the data of every link in list. Each item followed by a newline character. */
void list_print(virus *virus_list){
  virus *p = virus_list;
  
  while (p != NULL) {
    printf("Virus Name: %s\nVirus Size: %d\nSignature:\n",p->name,p->length);
    PrintHex(p->signature, p->length);
    printf("\n\n");
    p = p->next;
  } 
}
    
/* Add a new link with the given data to the list 
  (either at the end or the beginning, whichever you think is more convenient),
  and return a pointer to the list (i.e., the first link in the list).
  If the list is null - create a new entry and return a pointer to the entry. */
virus *list_append(virus *virus_list, virus *data){
  if (virus_list != NULL) {
    data->next = virus_list;
    return data;
  }

  return data;
}
     
/* Free the memory allocated by the list. */
void list_free(virus *virus_list){
  virus *p = virus_list;
  virus *temp;
  
  while (p != NULL) {
    temp = p;
    p = p->next;
    free(temp);
  } 
}


int main(int argc, char **argv) {
  FILE * pFile;
  long lSize;
  char * buffer;
  size_t result;
  int i;
  char * pos;
  virus *virus_list = NULL;

  pFile = fopen ( argv[1], "r" );
  if (pFile==NULL) {fputs ("File error",stderr); exit (1);}

  // obtain file size:
  fseek (pFile , 0 , SEEK_END);
  lSize = ftell (pFile);
  rewind (pFile);

  // allocate memory to contain the whole file:
  buffer = (char*) malloc (sizeof(char)*lSize);
  if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

  // copy the file into the buffer:
  result = fread (buffer,1,lSize,pFile);
  if (result != lSize) {fputs ("Reading error",stderr); exit (3);}
  
  pos = buffer;
  
  for (i=0; i < 10; i++) {
    int sigSize = *((int*)pos);
    char c;
    int nameSize = 0;
    virus *new_virus = (virus*) malloc (sizeof(virus));
    
    new_virus->next = NULL;
    
    new_virus->length = sigSize;

    new_virus->signature = pos + 4;

    new_virus->name = pos + 4 + sigSize;
    
    c = *(pos + 4 + sigSize); // first char
    
    while(c != 0) {
      ++nameSize;
      c = *(pos + 4 + sigSize + nameSize); // next char
    }
    
    // Appent to the list
    virus_list = list_append(virus_list, new_virus);
    
    pos = pos + 4 + sigSize + nameSize + 1; // update position
  }
  
  printf("List Print:\n");
  list_print(virus_list);
  
  list_free(virus_list);
  fclose (pFile);
  free (buffer);
  return 0;
}