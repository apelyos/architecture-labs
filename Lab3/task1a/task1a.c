#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

void PrintHex(buffer, length) {
  int i;
  unsigned char *byte = (unsigned char*)buffer;
  
  for(i = 0; i<length; ++i){
    printf("%02X " ,byte[i]);
  }
  
}

int main(int argc, char **argv) {
  FILE * pFile;
  long lSize;
  char * buffer;
  size_t result;
  int i;
  char * pos;

  pFile = fopen ( argv[1], "r" );
  if (pFile==NULL) {fputs ("File error",stderr); exit (1);}

  // obtain file size:
  fseek (pFile , 0 , SEEK_END);
  lSize = ftell (pFile);
  rewind (pFile);

  // allocate memory to contain the whole file:
  buffer = (char*) malloc (sizeof(char)*lSize);
  if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

  // copy the file into the buffer:
  result = fread (buffer,1,lSize,pFile);
  if (result != lSize) {fputs ("Reading error",stderr); exit (3);}
  
  pos = buffer;
  
  for (i=0; i < 10; i++) {
    int sigSize = *((int*)pos);
    char c;
    int nameSize = 0;
    printf("Virus Size: %d\n",sigSize);
    
    // print the virus sig
    printf("signature:\n");
    PrintHex(pos + 4, sigSize);
    printf("\n");
    
    // print virus name
    printf("Virus Name: ");
    c = *(pos + 4 + sigSize); // first char
    
    while(c != 0) {
      ++nameSize;
      printf("%c",c);
      c = *(pos + 4 + sigSize + nameSize); // next char
    }
    printf("\n\n");
    
    pos = pos + 4 + sigSize + nameSize + 1; // update position
  }
  
  printf("\n");
  
  fclose (pFile);
  free (buffer);
  return 0;
}