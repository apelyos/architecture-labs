#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

void PrintHex(buffer, length) {
  int i;
  unsigned char *byte = (unsigned char*)buffer;
  
  for(i = 0; i<length; ++i){
    printf("%02X " ,byte[i]);
  }
}


int main(int argc, char **argv) {
  FILE * pFile;
  long lSize;
  char * buffer;
  size_t result;

  pFile = fopen ( argv[1], "r" );
  if (pFile==NULL) {fputs ("File error",stderr); exit (1);}

  // obtain file size:
  fseek (pFile , 0 , SEEK_END);
  lSize = ftell (pFile);
  rewind (pFile);

  // allocate memory to contain the whole file:
  buffer = (char*) malloc (sizeof(char)*lSize);
  if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

  // copy the file into the buffer:
  result = fread (buffer,1,lSize,pFile);
  if (result != lSize) {fputs ("Reading error",stderr); exit (3);}
  
  PrintHex(buffer, lSize);
  /* the whole file is now loaded in the memory buffer. */

  printf("\n");
  
  fclose (pFile);
  free (buffer);
  return 0;
}