#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

typedef struct virus virus;

 
struct virus {
        int length;
        char *signature;
        char *name;
        virus *next;
};

void PrintHex(buffer, length) {
  int i;
  unsigned char *byte = (unsigned char*)buffer;
  
  for(i = 0; i<length; ++i){
    printf("%02X " ,byte[i]);
  }
  
}

 /* Print the data of every link in list. Each item followed by a newline character. */
void list_print(virus *virus_list){
  virus *p = virus_list;
  
  while (p != NULL) {
    printf("Virus Name: %s\nVirus Size: %d\nSignature:\n",p->name,p->length);
    PrintHex(p->signature, p->length);
    printf("\n\n");
    p = p->next;
  } 
}
    
/* Add a new link with the given data to the list 
  (either at the end or the beginning, whichever you think is more convenient),
  and return a pointer to the list (i.e., the first link in the list).
  If the list is null - create a new entry and return a pointer to the entry. */
virus *list_append(virus *virus_list, virus *data){
  if (virus_list != NULL) {
    data->next = virus_list;
    return data;
  }

  return data;
}
     
/* Free the memory allocated by the list. */
void list_free(virus *virus_list){
  virus *p = virus_list;
  virus *temp;
  
  while (p != NULL) {
    temp = p;
    p = p->next;
    free(temp);
  } 
}

int min(int a, int b) {
  if (a < b)
    return a;
  
  return b;
}


void detect_virus(char *buffer, virus *virus_list, unsigned int size) {
  virus *p = virus_list;
  //unsigned char *byte = (unsigned char*)buffer;
  unsigned int sizeToEnd;
  //unsigned int pos;
  int i;
  
  for(i = 0; i < size; ++i){
    
    while (p != NULL) {
      sizeToEnd = size - i;
      if (memcmp(buffer + i, p->signature, min(p->length, sizeToEnd)) == 0) {
	printf("VIRUS DETECTED!!!\n");
	printf("Starting byte: %d\n", i);
	printf("Virus Name: %s\n", p->name);
	printf("Virus Size: %d\n\n", p->length);
      }
      p = p->next;
    } 
    
    p = virus_list;
  }
}


int main(int argc, char **argv) {
  FILE * sFile;
  long lSize;
  char * buffer;
  char * virBuffer;
  int i;
  char * pos;
  virus *virus_list = NULL;
  
  if (argc != 3) {
    printf("USAGE: task1c <signature file> <suspected file>\n");
    return 0;
  }
  
  // Load the signature file + build virus_list: ****************************************

  sFile = fopen ( argv[1], "r" );
  if (sFile==NULL) {fputs ("File error\n",stderr); exit (1);}

  // obtain file size:
  fseek (sFile , 0 , SEEK_END);
  lSize = ftell (sFile);
  rewind (sFile);

  // allocate memory to contain the whole file:
  buffer = (char*) malloc (sizeof(char)*lSize);
  if (buffer == NULL) {fputs ("Memory error\n",stderr); exit (2);}

  // copy the file into the buffer:
  fread (buffer,1,lSize,sFile);
  
  pos = buffer;
  
  // Load signatures
  for (i=0; i < 10; i++) {
    int sigSize = *((int*)pos);
    char c;
    int nameSize = 0;
    virus *new_virus = (virus*) malloc (sizeof(virus));
    
    new_virus->next = NULL;
    new_virus->length = sigSize;
    new_virus->signature = pos + 4;
    new_virus->name = pos + 4 + sigSize;
    
    c = *(pos + 4 + sigSize); // first char
    
    while(c != 0) {
      ++nameSize;
      c = *(pos + 4 + sigSize + nameSize); // next char
    }
    
    // Appent to the list
    virus_list = list_append(virus_list, new_virus);
    
    pos = pos + 4 + sigSize + nameSize + 1; // next position
  }
  
  fclose (sFile);
  
  // Load the suspected file: ****************************************
  
  sFile = fopen ( argv[2], "r" );
  if (sFile==NULL) {fputs ("File error\n",stderr); exit (1);}

  // obtain file size:
  fseek (sFile , 0 , SEEK_END);
  lSize = ftell (sFile);
  rewind (sFile);

  // allocate memory to contain the whole file:
  virBuffer = (char*) malloc (sizeof(char)*10000);
  fread (virBuffer,1,min(lSize,10000),sFile);
  
  detect_virus(virBuffer,virus_list, min(lSize,10000));
  
  fclose (sFile);
  list_free(virus_list);
  free (buffer);
  free (virBuffer);
  
  return 0;
}