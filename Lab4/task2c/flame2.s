code_start:

global infection

section	.rodata
Text:
	DB	"Hello, Infected File", 10, 0	;  string

; prints to the screen the message "Hello, Infected File".
infection:
    push    ebp             ; Save caller state
    mov     ebp, esp
    sub     esp, 4          ; Leave space for local var on stack
    pushad                  ; Save some more caller state

    mov     eax, 4    	 	; arg1 = sys_write=4
    mov     ebx, 1   		; arg2 = file=stdout=1
    mov     ecx, Text   	; arg3 = pointer to text
    mov     edx, 22   		; arg4 = text size
    int     0x80            ; Transfer control to operating system

    popad                   ; Restore caller state (registers)
    mov     eax, [ebp-4]    ; place returned value where caller can see it
    add     esp, 4          ; Restore caller state
    pop     ebp             ; Restore caller state
    ret                     ; Back to caller

; opens the file named in its argument, 
; and adds the executable code from "code_start" to "code_end" 
; after the end of that file, and closes the file
infector: 
    push    ebp             ; Save caller state
    mov     ebp, esp
    sub     esp, 4          ; Leave space for local var on stack
    pushad                  ; Save some more caller state
    
    ;open:
    mov     eax, 5		;sys open
    mov     ebx, [ebp+8]    	; Copy function args to registers: leftmost...        
    mov     ecx, 1024		; o_append
    mov     edx, 0777		; permission
    
    int     0x80            ; Transfer control to operating system
    mov     [ebp-4], eax    ; Save returned value...
    
    popad                   ; Restore caller state (registers)
    mov     eax, [ebp-4]    ; place returned value where caller can see it
    add     esp, 4          ; Restore caller state
    pop     ebp             ; Restore caller state
    ret                     ; Back to caller


code_end: