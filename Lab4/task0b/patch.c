
#include "util.h"

#define SYS_WRITE 4
#define SYS_READ 3
#define SYS_OPEN 5
#define SYS_CLOSE 6
#define SYS_LSEEK 19
#define SYS_EXIT 1
#define STDOUT 1
#define O_RDWR 2

void quit_err() {
  system_call(SYS_WRITE,STDOUT, "err\n", strlen("err\n"));
  system_call(SYS_EXIT, 55);
}

int main (int argc , char* argv[], char* envp[]) {
  /* need to patch 0x291 */
  int result;
  int fileDesc = system_call(SYS_OPEN, "greeting", O_RDWR, 0777);
  
  if (argc != 2) {
    quit_err();
  }
  
  if (fileDesc == -1) {
    quit_err();
  }
  
  result = system_call(SYS_LSEEK, fileDesc, 0x291, 0); /* 0 = from start of file */
  
  if (result == -1) {
    quit_err();
  }
  
  result = system_call(SYS_WRITE, fileDesc, argv[1], strlen(argv[1]) + 1);
  
  if (result == -1) {
    quit_err();
  }
  
  system_call(SYS_CLOSE, fileDesc);
  
  return 0;
}

