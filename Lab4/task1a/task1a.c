/*#include <stdlib.h>
#include <stdio.h>*/

#include "util.h"

#define SYS_WRITE 4
#define SYS_READ 3
#define SYS_OPEN 5
#define SYS_CLOSE 6
#define SYS_LSEEK 19
#define SYS_EXIT 1
#define O_RDWR 2
#define EOF -1
#define stdin 0
#define stdout 1
#define stderr 2

void print(char* str) {
  system_call(SYS_WRITE, stdout, str, strlen(str));
}

void quit_err() {
  print("Err");
  system_call(SYS_EXIT, 55);
}

char fgetc(int file) {
  char chr;
  int result;
  
  result = system_call(SYS_READ, file, &chr, 1);
  
  if (result == -1) {
    quit_err();
  }
  
  if (chr == 4 || chr == 0) {
    return -1;
  }
  
  /*print("getting:");
  print (itoa(chr));*/
  return chr;
}

int main(int argc, char **argv) {
  char c;
  int line = 1;
  int splitByChar = -1;
  int splitByDigit = 0;
  int input = stdin;
  
  
  /* Reading params 
  for(i=1; i<argc; i++){
    if(strcmp(argv[i], "-s") == 0)
	splitByChar = argv[++i][0];
    else if(strcmp(argv[i], "-d") == 0)
	splitByDigit = 1;
    else if(strcmp(argv[i], "-i") == 0)
	input = fopen(argv[++i],"r");
    else {
	printf("invalid parameter - %s\n",argv[i]);
	return 1;
    }
  }*/
  
  /* Starting to split stuff */
  c = fgetc(input);
  print(itoa(line));
  print(":");
  /*printf("%d:",line);*/

    
  while (c != EOF) {
    print(&c);
    /*printf("%c",c);*/

    
    switch(c) {
      case '@':
      case '*':
	line++;
	print("\n");
	print(itoa(line));
	print(":");
	/*printf("\n%d:",line);*/
	break;
	
      default:
	    if (splitByChar != -1 && c == splitByChar) {
	      line++;
	      print("\n");
	      print(itoa(line));
	      print(":");
	    } else if (splitByDigit && c >= 48 && c <= 57) {
		line++;
		print("\n");
		print(itoa(line));
		print(":");
	    }
      break;
    }
    
    /* read next */
    c = fgetc(input);
  } 
  
  return 0;
}