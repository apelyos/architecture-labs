/*#include <stdlib.h>
#include <stdio.h>*/

#include "util.h"

#define SYS_WRITE 4
#define SYS_READ 3
#define SYS_OPEN 5
#define SYS_CLOSE 6
#define SYS_LSEEK 19
#define SYS_EXIT 1
#define O_RDWR 2
#define O_RD 0
#define O_WR 1
#define O_CREATE 64
#define EOF -1
#define stdin 0
#define stdout 1
#define stderr 2

void fprint(int file, char* str) {
  system_call(SYS_WRITE, file, str, strlen(str));
}

void print(char* str) {
  fprint(stdout, str);
}

void fprintc(int file, char c) {
  system_call(SYS_WRITE, file, &c, 1);
}

void quit_err(char *msg) {
  print(msg);
  system_call(SYS_EXIT, 55);
}

char fgetc(int file) {
  char chr;
  int result;
  
  result = system_call(SYS_READ, file, &chr, 1);
  
  if (result == -1) {
    quit_err("Read error\n");
  }
  
  if (chr == 4 || chr == 3 || chr == 0 || chr == 10) {
    return -1;
  }
  
  return chr;
}

void gets(char *str) {
  char chr;
  int i = 0;
  
  chr = fgetc(stdin);
  
  while (chr != -1 && chr != '\n') {
    str[i] = chr;
    chr = fgetc(stdin);
    i++;
  }
}

int fopen(char *file, int op) {
  int fileDesc = system_call(SYS_OPEN, file, op, 0777);
  
  if (fileDesc < 0) {
    quit_err("File open error\n");
  }
  
  return fileDesc;
}

void fclose(int file) {
  system_call(SYS_CLOSE, file);
}

int main(int argc, char **argv) {
  char c;
  int i;
  int line = 1;
  int splitByChar = -1;
  int splitByDigit = 0;
  int outputToFiles = 0;
  int currentFile = 0;
  int isNext = 0;
  char fileName[10];
  int output[5];
  int input = stdin;
  
  output[0] = stdout;
  
  for(i=1; i<argc; i++){
    if(strcmp(argv[i], "-s") == 0)
	splitByChar = argv[++i][0];
    else if(strcmp(argv[i], "-d") == 0)
	splitByDigit = 1;
    else if(strcmp(argv[i], "-i") == 0)
	input = fopen(argv[++i],O_RD);
    else if(strcmp(argv[i], "-o") == 0)
	outputToFiles = positive_atoi(argv[++i]);
    else {
	print("invalid parameter\n");
	return 1;
    }
  }
  
  for(i=0; i < outputToFiles; i++){
    print("Enter file name:\n");
    gets(fileName); 
    output[i] = fopen(fileName, O_WR | O_CREATE); 
  }
  
  c = fgetc(input);
  fprint(output[currentFile],itoa(line));
  fprint(output[currentFile],":");
  /*fprintf(output[currentFile],"%d:",line);*/
  
  while (c != EOF) {
   
    fprintc(output[currentFile],c);
    
    switch(c) {
      case '@':
      case '*':
	isNext = 1;
	break;
	
      default:
	    if (splitByChar != -1 && c == splitByChar) {
	      isNext = 1;
	    } else if (splitByDigit && c >= 48 && c <= 57) {
		isNext = 1;
	    }
      break;
    }
    
    if (isNext) {
      line++;
      fprint(output[currentFile],"\n");
      
      if (outputToFiles != 0)
	currentFile = ((line - 1)  % outputToFiles); /*Cycle through files*/
	
        fprint(output[currentFile],itoa(line));
	fprint(output[currentFile],":");
    }
    
    c = fgetc(input);
    isNext = 0;
    
    print(itoa(c));
    print ("\n");
  } 
  
   for(i=0; i < outputToFiles; i++){
     fclose(output[i]);
  }
  
  return 0;
}